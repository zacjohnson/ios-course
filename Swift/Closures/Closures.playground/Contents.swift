import UIKit

// Here is a normal addition function. It takes two numbers as inputs then adds them together.

func calculator(n1: Int, n2: Int, operation: (Int, Int) -> Int) -> Int {
	return operation(n1, n2)
}

/**
	Now whichever operation is passed into the calculator function, will be used by
	the calculator
**/

let result = calculator(n1: 2, n2: 3, operation: { (no1, no2) in
	no1 + no2
})

let result2 = calculator(n1: 4, n2: 5, operation: {$0 + $1})

/**
	We can add the other three functions as well:
**/


