//
//  main.swift
//  Classes and Objects
//
//  Created by Zac Johnson on 4/30/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import Foundation

let myCar = Car()

let someRichGuysCar = Car(customerChosenColor: "Gold")

print(myCar.color)
print(myCar.numberOfSeats)
print(myCar.typeOfCar)

print(someRichGuysCar.color)
print(someRichGuysCar.numberOfSeats)
print(someRichGuysCar.typeOfCar)

myCar.drive()

let mySelfDrivingCar = SelfDrivingCar()
// mySelfDrivingCar.destination = "1 Infinite Loop"
mySelfDrivingCar.drive()
