//
//  Car.swift
//  Classes and Objects
//
//  Created by Zac Johnson on 4/30/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import Foundation

enum CarType {
	case Sedan
	case Coupe
	case Hatchback
}

class Car {
	var color = "Black"
	var numberOfSeats = 5
	var typeOfCar : CarType = .Coupe // Use "." to access the enumeration
	
	/**
	 * Designated Initializer: A compulsory initializer that you must use to create a Car instance
	 * Basically a default constructor
	 */
	init() {
		
	}
	
	convenience init(customerChosenColor : String) {
		self.init() // Initialize default properties of Car
		color = customerChosenColor
	}
	
	func drive() {
		print("vroom vroom")
	}
}
