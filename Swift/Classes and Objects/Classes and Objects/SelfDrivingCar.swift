//
//  SelfDrivingCar.swift
//  Classes and Objects
//
//  Created by Zac Johnson on 4/30/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import Foundation

// SelfDrivingCar is the subclass of the Car superclass
class SelfDrivingCar : Car {
	
	// Destination can be a String or nil
	var destination : String?
	
	// Car superclass already has a drive method
	override func drive() {
		super.drive()
		
		// Optional binding - No force unwrapping needed!
		if let userSetDestination = destination {
			print("Driving towards " + userSetDestination)
		} else {
			print("\"Recalculating\"")
		}
		
//		if destination != nil {
//			// Force unwrapping destination
//			print("Driving to " + destination!) // ! says that "I swear on my life that destination will never be nil
//		}
	}
}
