//
//  AppDelegate.swift
//  Todoey
//
//  Created by Zac Johnson on 2/6/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		// Location of Realm database file
		// print(Realm.Configuration.defaultConfiguration.fileURL)
		
		
		do {
			_ = try Realm()
		} catch {
			print("Error initializing Realm: \(error)")
		}
		
		
		return true
	}

}

