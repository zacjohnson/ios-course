//
//  CategoryViewController.swift
//  Todoey
//
//  Created by Zac Johnson on 6/7/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import UIKit
import RealmSwift

class CategoryViewController: SwipeTableViewController {
	
	// TODO: Initiate instance of Realm
	let realm = try! Realm()
	
	// TODO: Collection of categories that the realm returns
	var categories : Results<Category>?

    override func viewDidLoad() {
        super.viewDidLoad()
		
		loadCategories()
    }

	// MARK: TableView Datasource Methods
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// TODO: Return number of rows, but if it's 0, return 1
		return categories?.count ?? 1    // nil coalescing operator
	}
	
	// Display the categories in the container
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		// TODO: Grab cell from superclass
		let cell = super.tableView(tableView, cellForRowAt: indexPath)
		
		cell.textLabel?.text = categories?[indexPath.row].name ?? "No Categories Added Yet"
		
		return cell
	}
	
	// MARK: Data Manipulation Methods

	func save(category: Category) {
		do {
			try realm.write {
				realm.add(category)
			}
		} catch {
			print("Error saving category conxtext: \(error)")
		}
		
		// Update TableView with data from array
		self.tableView.reloadData()
	}
	
	func loadCategories() {
		// TODO: Look in realm and grab all objects belong to the Category Datatype
		categories = realm.objects(Category.self)

		tableView.reloadData()
	}
	
	// MARK: Delete data from Swipe
	
	// TODO: Override updateModel method from superclass
	override func updateModel(at indexPath: IndexPath) {
		if let categoryForDeletion = self.categories?[indexPath.row] {
			do {
				try self.realm.write {
					self.realm.delete(categoryForDeletion)
				}
			} catch {

			}
		}
	}
	
	// MARK: Add New Categories
	
	@IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
		var textField = UITextField()
		
		// Create alert box
		let alert = UIAlertController(title: "Add New Category", message: "", preferredStyle: .alert)
		
		// Add an "Add item" button
		let action = UIAlertAction(title: "Add Category", style: .default) {
			(action) in
			
			let newCategory = Category()
			newCategory.name = textField.text!
			
			self.save(category: newCategory)
		}
		
		// Add textField to Alert box
		alert.addTextField { (alertTextField) in
			alertTextField.placeholder = "Add a new category"
			textField = alertTextField
		}
		
		alert.addAction(action)
		
		present(alert, animated: true, completion: nil)
	}
	
	
	
	// MARK: TableView Delegate Methods
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "goToItems", sender: self)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let destinationVC = segue.destination as! TodoListViewController
		
		if let indexPath = tableView.indexPathForSelectedRow {
			destinationVC.selectedCategory = categories?[indexPath.row]
		}
	}
}
