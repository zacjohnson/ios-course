//
//  ViewController.swift
//  Todoey
//
//  Created by Zac Johnson on 2/6/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import UIKit
import RealmSwift

class TodoListViewController: SwipeTableViewController {
	
	var todoItems: Results<Item>?
	let realm = try! Realm()
	
	var selectedCategory : Category? {
		didSet {
			loadItems()
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		print(dataFilePath)
	}
	
	// MARK: TableView Datasource Methods
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return todoItems?.count ?? 1
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = super.tableView(tableView, cellForRowAt: indexPath)
		
		if let item = todoItems?[indexPath.row] {
			cell.textLabel?.text = item.title
			
			// Note: Set checkmark if done property is true else take the checkmark away
			cell.accessoryType = item.done ? .checkmark : .none
		} else {
			cell.textLabel?.text = "No Items Added"
		}
		
		return cell
	}
	
	// MARK: TableView Delegate Methods
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let item = todoItems?[indexPath.row] {
			do {
				try realm.write {
					item.done = !item.done
				}
			} catch {
				print("Error sving done status: \(error)")
			}
		}
		
		tableView.reloadData()
		
		//TODO: Darken cell when selected then return to original color
		tableView.deselectRow(at: indexPath, animated: true)
	}
	
	// MARK: Add New Items
	
	@IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
		var textField = UITextField()
		
		// Create alert box
		let alert = UIAlertController(title: "Add New Todoey Item", message: "", preferredStyle: .alert)
		
		// Add an "Add item" button
		let action = UIAlertAction(title: "Add Item", style: .default) {
			(action) in
			
			if let currentCategory = self.selectedCategory {
				do {
					try self.realm.write {
						let newItem = Item()
						newItem.title = textField.text!
						newItem.dateCreated = Date()
						currentCategory.items.append(newItem)
					}
				} catch {
					print("Error saving new items: \(error)")
				}
			}
			
			// Update TableView with data from array
			self.tableView.reloadData()
	
		}
		
		// Add textField to Alert box
		alert.addTextField { (alertTextField) in
			alertTextField.placeholder = "Create a new item"
			textField = alertTextField
		}
		
		alert.addAction(action)
		
		present(alert, animated: true, completion: nil)
	}
	
	// MARK: Model Manipulation Methods
	
	// load up item array
	func loadItems() {
		
		todoItems = selectedCategory?.items.sorted(byKeyPath: "title", ascending: true)

		tableView.reloadData()
	}
	
	override func updateModel(at indexPath: IndexPath) {
		if let item = todoItems?[indexPath.row] {
			
			do {
				try realm.write {
					realm.delete(item)
				}
			} catch {
				print(error)
			}
		}
	}
}

// MARK: Search Bar Methods

extension TodoListViewController: UISearchBarDelegate {
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		todoItems = todoItems?.filter("title CONTAINS[cd] %@", searchBar.text).sorted(byKeyPath: "dateCreated", ascending: true)
		
		tableView.reloadData()
	}

	

	// TODO: Go back to the original list when clicking the "x"
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchBar.text?.count == 0 {
			loadItems()

			// TODO: Assigns tasks to different thread
			DispatchQueue.main.async {
				// TODO: Make the keyboard + cursor go away
				searchBar.resignFirstResponder()
			}

		}
	}
}

