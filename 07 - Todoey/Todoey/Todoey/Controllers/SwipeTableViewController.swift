//
//  SwipeTableViewController.swift
//  Todoey
//
//  Created by Zac Johnson on 6/8/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import UIKit
import SwipeCellKit

// SUPERCLASS
class SwipeTableViewController: UITableViewController, SwipeTableViewCellDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.rowHeight = 80.0
    }
	
	// MARK: TableView Datasource Methods
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
		
		cell.delegate = self
		
		return cell
	}

	func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
		
		// Check orientation of swipe
		guard orientation == .right else { return nil }
		
		// What happens when cell is swiped
		let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
			// handle action by updating model with deletion
			
			self.updateModel(at: indexPath)
		}
		
		// customize the action appearance
		deleteAction.image = UIImage(named: "delete-icon")
		
		return [deleteAction]
	}
	
	func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
		var options = SwipeOptions()
		options.expansionStyle = .destructive
		return options
	}
	
	func updateModel(at indexPath: IndexPath) {
		// Update data model
	}


}

