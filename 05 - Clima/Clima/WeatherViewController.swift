import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class WeatherViewController: UIViewController, CLLocationManagerDelegate, ChangeCityDelegate {
    
    //Constants
    let WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
    let APP_ID = "037e6297f594d5f197e930e5f1ea0297"
    

    //TODO: Declare instance variables here
    let locationManager = CLLocationManager()
	let weatherDataModel = WeatherDataModel()
    
    //Pre-linked IBOutlets
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //TODO: Set up the location manager here.
    	locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    
    
    //MARK: - Networking
    /***************************************************************/
    
    //Write the getWeatherData method here:
	func getWeatherData(url: String, parameters: [String : String]) {
		Alamofire.request(url, method: .get, parameters: parameters).responseJSON {
			response in
			if response.result.isSuccess {
				print("Success, weather data obtained!")
				
				let weatherJSON : JSON = JSON(response.result.value!)
				self.updateWeatherData(json: weatherJSON)
				
			} else {
				print(String(describing: response.result.error))
				self.cityLabel.text = "Connection Interrupted"
			}
		}
	}

    
    
    
    
    
    //MARK: - JSON Parsing
    /***************************************************************/
   
    
    //Write the updateWeatherData method here:
	func updateWeatherData(json: JSON) {
		// Optional Binding
		if let tempResult = json["main"]["temp"].double {
			weatherDataModel.temperature = Int((tempResult - 273.15) * 1.8 + 32)
			
			weatherDataModel.city = json["name"].stringValue
			
			weatherDataModel.condition = json["weather"][0]["id"].intValue
			
			weatherDataModel.weatherIconName = weatherDataModel.updateWeatherIcon(condition: weatherDataModel.condition)
			
			updateUIWithWeatherData()
		} else {
			cityLabel.text = "Weather Unavailable"
		}
	}

    
    
    
    //MARK: - UI Updates
    /***************************************************************/
    
    
    //Write the updateUIWithWeatherData method here:
	func updateUIWithWeatherData() {
		cityLabel.text = weatherDataModel.city
		temperatureLabel.text = String(weatherDataModel.temperature) + "°"
		weatherIcon.image = UIImage(named: weatherDataModel.weatherIconName)
	}
    
    
    
    
    
    //MARK: - Location Manager Delegate Methods
    /***************************************************************/
    
    
    //Write the didUpdateLocations method here:
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let location = locations[locations.count - 1]
		if location.horizontalAccuracy > 0 {
			locationManager.stopUpdatingLocation() // This saves the user's battery!
			
			print("Longitude: \(location.coordinate.longitude), latitude = \(location.coordinate.latitude)")
			
			let latitude = String(location.coordinate.latitude)
			let longitude = String(location.coordinate.longitude)
			
			// Dictionary w/ String key and String value [explicit]
			let params : [String : String] = ["lat" : latitude, "lon" : longitude, "appid" : APP_ID]
			
			getWeatherData(url: WEATHER_URL, parameters: params)
		}
	}
    
    
    //Write the didFailWithError method here:
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print(error)
		cityLabel.text = "Location Unavailable"
	}
    
    

    
    //MARK: - Change City Delegate methods
    /***************************************************************/
    
    
    //Write the userEnteredANewCityName Delegate method here:
	func userEnteredANewCityName(city: String) {
		let params : [String : String] = ["q" : city, "appid" : APP_ID]
		
		getWeatherData(url: WEATHER_URL, parameters: params)
	}

    
    //Write the PrepareForSegue Method here
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "changeCityName" {
			let destinationVC = segue.destination as! ChangeCityViewController
			
			destinationVC.delegate = self
		}
	}
    
    
    
    
}


