//
//  Question.swift
//  Quizzler
//
//  Created by Zac Johnson on 4/22/18.
//  Copyright © 2018 London App Brewery. All rights reserved.
//

import Foundation

class Question {
	
	let questionText : String
	let answer : Bool
	
	// Initializer/Constructor
	init(text: String, correctAnswer: Bool) {
		questionText = text
		answer = correctAnswer
	}
}
