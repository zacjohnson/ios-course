iOS Repository - Zac Johnson

### These are project I've created while learing iOS development from the "iOS 11 & Swift 4 - The Complete iOS App Development Bootcamp" course by Angela Yu on Udemy.com. 

These will range from easy to advanced difficulty, with some apps being incredibly basic and others using APIs/Cloud storage.

Projects increase in difficulty from 1-7. Todoey makes use of Realm, and FlashChat is using Firebase.

The Coding Challenges are other implementations of the original projects.