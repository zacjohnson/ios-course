//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Zac Johnson on 4/21/18.
//  Copyright © 2018 Zac Johnson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	var randomNumber : Int = 0
	
	let ballArray = ["ball1", "ball2", "ball3", "ball4", "ball5"]
	
	@IBOutlet weak var magic8BallImageView: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		updateMagic8BallImage()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@IBAction func askButtonPressed(_ sender: UIButton) {
		updateMagic8BallImage()
	}
	
	func updateMagic8BallImage() {
		randomNumber = Int(arc4random_uniform(5))
		
		magic8BallImageView.image = UIImage(named: ballArray[randomNumber])
	}
	
	override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
		updateMagic8BallImage()
	}
	
}

